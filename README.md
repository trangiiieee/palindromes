# Palindrome test

Spiderman must save Maryjane from the horrible PALINDROMICON. To defeat the PALINDROMICON he must produce 50 palindromes. A palindrome is a word/word phrase that is the same backwards as it is forwards.

This program will log the first 50 palindromes based on the JSON data from https://github.com/dwyl/english-words

## Install

`npm install`

## Run program

`npm run start`
