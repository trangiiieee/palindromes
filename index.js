const axios = require("axios");

getPalindromes();

/**
 * get 50 palindromes based off a github list
 */
async function getPalindromes() {
  try {
    // get words from github link
    const words = await getResults();

    // turn object's keys into an array
    const wordsArray = Object.keys(words);

    // get all palindromes
    const palindromes = wordsArray.filter(palinDromesTest);

    // limit palindromes to 50
    const limitPalindromes = palindromes.slice(0, 50);

    console.log(limitPalindromes);
  } catch (e) {
    console.error(e);
  }
}

/**
 * get a list of words
 */
async function getResults() {
  const results = await axios.get(
    "https://raw.githubusercontent.com/dwyl/english-words/master/words_dictionary.json"
  );

  return results.data;
}

/**
 * Palindromes are the same backwords as it is forwards.
 * If a word is reversed and equals the word, then it's a palindrome!
 *
 * @param {string} word
 * @return bool
 */
function palinDromesTest(word) {
  const backWord = word
    .split("")
    .reverse()
    .join("");

  return word === backWord;
}
